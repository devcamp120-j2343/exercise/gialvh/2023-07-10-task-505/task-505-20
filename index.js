//import thư viện expressjs 
const express = require("express");

//khởi tạo 1 app express
const app = express();

//khai báo cổng chạy 
const port = 8000;

//callback function là 1 function đóng bai trò là tham số của 1 function khác, nó sẽ được thực thi khi function chủ được gọi
//khai báo API dạng /
app.get("/",(req,res)=>{

})

//khai báo api dạng GET
app.get("/get-method",(req,res)=>{
    res.json({
        message :"GET method"
    })
})

//khai báo API dạng POST
app.post("/post-method",(req,res) => {
    res.json({
        message : "POST method"
    })
})

//khai báo API dạng PUT
app.put("/put-method",(req,res) => {
    res.json({
        message : "PUT method"
    })
})

//khai báo API dạng DELETE
app.delete("/delete-method",(req,res) => {
    res.json({
        message : "DELETE method"
    })
})

app.listen(port,() => {
    console.log("app listening on port :" , port);
})